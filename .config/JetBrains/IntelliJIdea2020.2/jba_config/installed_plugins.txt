A move tab left and right using the keyboard plugin - by momomo.com
A prevent pinned tabs from closing plugin - by momomo.com
Axis TCP Monitor Plugin
BrowseWordAtCaret
Code Navigator
Dilbert
Force Shortcuts
GrepConsole
Key Promoter X
Lombook Plugin
PegdownDocletIdea
PlantUML integration
String Manipulation
com.daylerees.rainglow
com.denis.zaichenko.angular.2.ws.live.templates
com.github.b3er.idea.plugins.arc.browser
com.intellij.bigdecimal-folding
com.handyedit.ant.AntDebugger
com.j92.current-date-generator
com.jetbrains.malenkov.color.blindness.support
com.jetbrains.php.framework
com.intellij.plugins.watcher
com.neon.intellij.plugins.gitlab
com.ppolivka.gitlabprojects
com.widerwille.afterglow
de.kontext_e.idea.plugins.autofill
de.netnexus.camelcaseplugin
fr.dco.kotlin.vcs-kotlin-converter
me.guichaguri.additionaltools
mobi.hsz.idea.gitignore
net.masterthought.dlanguage
net.vektah.codeglance
one.util.ideaplugin.screenshoter
org.asciidoctor.intellij.asciidoc
AutoPackage
org.jetbrains.erlang
apache-felix-plugin
io.gulp.intellij
io.snyk.snyk-intellij-plugin
BashSupport
NodeJS
com.intellij.plugins.html.instantEditing
com.wix.coffeelint
org.intellij.scala
com.github.danielwegener.cucumber-scala
org.jetbrains.plugins.go
org.jetbrains.plugins.vue
Dart
Pythonid
com.intellij.kubernetes
org.jetbrains.plugins.ruby
org.rust.lang
ru.adelf.idea.dotenv
uk.co.ben-gibson.remote.repository.mapper