__module_name__	= "MOCP_info"
__module_version__	= "1"
__module_description__	= "MOCP Info Script"
__module_author__	=	"kubicz10"

# Personalize this plugin by editing source code.
# I release this under terms of GPL license, http://www.gnu.org/copyleft/gpl.html

# Known commands:
# mocp         :displays current song information in to the current channel
# mocp_local   :prints song information for you
# mocp_stop    :stops mocp from playing
# mocp_play    :start mocp
# mocp_next    :plays next song
# mocp_prev    :plays previous song
# mocp_toggle  :toggles between play and pause
# mocp_exit    :shutdown mocp.
import xchat
import commands

def mocp_info_full(word,word_eol,userdata):
#Info Full#
        notitle = commands.getoutput("mocp -i|grep -w Title:|wc -w")
        nttitle = commands.getoutput("mocp -i|grep -w File:|awk -F/ '{print $NF}'")
        noalbum = commands.getoutput("mocp -i|grep -w Album:|wc -w")
        stop = commands.getoutput("mocp -i |tail -n1|sed 's/State://g'|sed 's/ //g'")
        artist = commands.getoutput("mocp -i|grep -w Artist:|sed 's/Artist://g'")
	album = commands.getoutput("mocp -i|grep -w Album:|sed 's/Album://g'")
        title = commands.getoutput("mocp -i|grep -w SongTitle:|sed 's/SongTitle://g'")
   	ttime = commands.getoutput("mocp -i|grep -w TotalTime:|sed 's/TotalTime://g'|sed 's/ //g'")
        tcurrent = commands.getoutput("mocp -i|grep -w CurrentTime:|sed 's/CurrentTime://g'|sed 's/ //g'")
	if notitle == "1": xchat.command("say MOCP playing:\002 %s \002 (notag)" % (nttitle))
	else:   
		if noalbum == "1": xchat.command("say MOCP playing:\002%s - %s - no album - %s/%s\002" % (artist, title, tcurrent, ttime)) 
	        else: 
                	if stop == "STOP": print("mocp isnt running")
        		else: title == xchat.command("say MOCP playing:\002%s - %s - %s - %s/%s\002" % (artist, title, album, tcurrent, ttime))       
        return xchat.EAT_ALL
xchat.hook_command("mocp", mocp_info_full, help="Prints MOCP info public")

def mocp_local_info(word,word_eol,userdata):
#Info Local#
        stop = commands.getoutput("mocp -i |tail -n1|sed 's/State://g'|sed 's/ //g'")
        notitle = commands.getoutput("mocp -i|grep -w Title:|wc -w")
        nttitle = commands.getoutput("mocp -i|grep -w File:|awk -F/ '{print $NF}'")
        title = commands.getoutput("mocp -i|grep -w Title:|sed 's/Title://g'")
        if notitle == "1": xchat.command("say MOCP playing: %s (notag)" % (nttitle))
        else: 
		if stop == "STOP": print("mocp isnt running") 
        	else: print("you are listening to %s" % title)
        return xchat.EAT_ALL
xchat.hook_command("mocp_local", mocp_local_info, help="Prints MOCP info local")

def mocp_stop_playing(word,word_eol,userdata):
#Stop playing#
	stop = commands.getoutput("mocp -i |tail -n1|sed 's/State://g'|sed 's/ //g'")
        if stop == "STOP": print ("mocp is stoped or not running") 
        else:
        	commands.getoutput("mocp -s")
        	print("stopping mocp")
        return xchat.EAT_ALL
xchat.hook_command("mocp_stop", mocp_stop_playing, help="Stop playing.")

def mocp_play(word,word_eol,userdata):
#Start playing#	
        play = commands.getoutput("mocp -i |head -n1|sed 's/State://g'|sed 's/ //g'")
        if play == "PLAY": print ("mocp is playing")
        else: 
        	commands.getoutput("mocp -p")
                print("mocp starts playing")
        return xchat.EAT_ALL
xchat.hook_command("mocp_play", mocp_play, help="Start playing from the first item on the playlist.")

def mocp_next(word,word_eol,userdata):
#Next song#
	commands.getoutput("mocp -f")
        print("mocp jumps to next song")
        return xchat.EAT_ALL
xchat.hook_command("mocp_next", mocp_next, help="Play next song.")

def mocp_previous(word,word_eol,userdata):
#Previous song#
	commands.getoutput("mocp -r")
	print("mocp jumps to previous song")
	return xchat.EAT_ALL
xchat.hook_command("mocp_prev", mocp_previous, help="Play previous song.")

def mocp_toggle(word,word_eol,userdata):
#Toggle play/pause#
	pause = commands.getoutput("mocp -i |head -n1|sed 's/State://g'|sed 's/ //g'")
	if pause == "PAUSE": 
        	commands.getoutput("mocp -G")
        	print("mocp unpaused")
	else:
		if pause == "PLAY": 
			commands.getoutput("mocp -G")
			print("mocp paused")
		else: print("mocp is stoped or isnt running")
	return xchat.EAT_ALL
xchat.hook_command("mocp_toggle", mocp_toggle, help="Toggle between play/pause.")

def mocp_exit(word,word_eol,userdata):
#Exit mocp#
	commands.getoutput("mocp -x")
	print("mocp is off")
	return xchat.EAT_ALL
xchat.hook_command("mocp_exit", mocp_exit, help="Shutdown MOCP.")	

print	"X-Chat2 MOCP Info Script"		   









