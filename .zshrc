# vim:nospell:foldmethod=marker:foldmarker={{,}}:

export TERM=xterm-256color
export TZ=Europe/Berlin
export EDITOR=vim
export LESS="$LESS -J"
export GREP_COLOR='01;32'
export MANPATH=~/local/share/man:/usr/local/share/man:$MANPATH
export PATH=~/bin:/usr/local/sbin:/usr/local/bin:/sbin:/usr/sbin:/bin:/usr/bin:~/local/bin:$PATH:~/bin
export MAILDIR=~/.Mail
export TEXMFHOME=~/.texmf
export BROWSER=google-chrome

export LANG=de_DE.utf8
export LC_MESSAGES=C

JAVA_HOME=/usr/lib/jvm/java-8-jdk
M2_HOME=/opt/maven
ANT_HOME=/usr/share/java/apache-ant
GRADLE_HOME=/usr/share/java/gradle
PATH=~/bin:$JAVA_HOME/bin:$M2_HOME/bin:$ANT_HOME/bin:$GRADLE_HOME/bin:$PATH

autoload -U colors
autoload -U compinit
autoload -U promptinit
#autoload bashcompinit
#bashcompinit
colors
compinit
promptinit
setopt NULL_GLOB
setopt auto_pushd
setopt extendedglob
setopt kshglob
setopt prompt_subst
setopt share_history

# Set a nice git-prompt (calls git_status()) {{
git_prompt_info() {
  local git_dir ref br;
  git_dir=$(git rev-parse --git-dir 2> /dev/null) || return
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  branch_prompt=${ref#refs/heads/}
  if [ -n "$branch_prompt" ]; then
    if [[ $PWD != *".git"* ]]; then
    	status_icon=$(git_status)
    fi
    echo "$status_icon%{$fg[yellow]%} [$branch_prompt]"
  fi
}

# Show character if index and/or work-tree are dirty
git_status() {
  git diff --cached --quiet
  if [[ $? == 1 ]]; then
    output="%{$fg[green]%}+"
  fi
  
  git diff --quiet
  if [[ $? == 1 ]]; then
    output="$output%{$fg[red]%}*"
  fi
  echo $output
} # }}

# {{
typeset -A HOST_COLORS
HOST_COLORS=(www cyan datenbank magenta fileserver blue guardian green)

export RPS1='$(git_prompt_info) %{$reset_color%}%*'
if [[ "$USER" == "root" ]] ; then
	export PS1='[%?]%{$fg[red]%}%n%{$reset_color%}@%m:%~# '
else
	if [[ -n "$SSH_CLIENT" ]]; then
		export PS1='[%?]%{$fg[green]%}%n%{$reset_color%}@%{$fg[yellow]%}%m%{$reset_color%}:%~\$ '
	else
		export PS1='[%?]%{$fg[green]%}%n%{$reset_color%}@%m:%~\$ '
	fi

	if [[ -n "$HOST_COLORS[$(hostname)]" ]]; then
		export PS1='[%?]%{$fg[green]%}%n%{$reset_color%}@%{$fg[$HOST_COLORS[$hostname)]]%}%m%{$reset_color%}:%~\$ '
	fi
fi

local _myhosts
_myhosts=( ${${${${(f)"$(<$HOME/.ssh/known_hosts)"}:#.*}%%\ *}%%,*} )
zstyle ':completion:*:(ssh|scp)' hosts $_myhosts
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' menu select=3

# source the bash-completion for the newest git
#if [ -f ~/.git-completion.bash ] ; then
#	source ~/.git-completion.bash
#fi #}}

# mapping of functions keys# {{
bindkey '^A'    beginning-of-line       # Home
bindkey '^E'    end-of-line             # End
bindkey '^D'    delete-char             # Del
bindkey '^R'	history-incremental-search-backward 	# reverse history search
bindkey '^[[3~' delete-char             # Del
bindkey '^[[2~' overwrite-mode          # Insert
bindkey '^[[5~' history-search-backward # PgUp
bindkey '^[[6~' history-search-forward  # PgDn
bindkey "\e\e[D" backward-word
bindkey "\e\e[C" forward-word # }}

HISTFILE="$HOME/.zsh_history"
HISTSIZE=5000
SAVEHIST=5000

SEVEN_ZIP_BIN_PATH="/cygdrive/c/Program\ Files/7-Zip"
VBOX_BIN_PATH="/cygdrive/c/Program\ Files/Oracle/VirtualBox"
VBOX_VM_PATH="/cygdrive/c/Users/$LOGNAME/VirtualBox\ VMs"

source ~/.zsh/aliases.ubuntu

mutti () { mutt -F ~/.mutt/profile.$* }

pdfgrep () {
	for file in $*; do
		echo "=== $file ==="
		pdftotext $file - | grep --color -i $1
	done
}

git_sync () {
	CURDIR=$PWD
	for dir in $(find $* -name "*.git" -type d)
	do
		cd $CURDIR/$dir/../ && if [[ -n $(git remote) ]]
		then
			echo "*** Pulling in dir $PWD" && git pull -q && git gc --quiet; 
		fi
	done
	cd $CURDIR
	echo "*** All done!"
}

git_gc () {
	CURDIR=$PWD
	for dir in $(find $* -name "*.git" -type d)
	do
		cd $CURDIR/$dir/../
		echo "*** GC in dir $PWD" && git gc --quiet
	done
	cd $CURDIR
	echo "*** All done!"
}

source ~/.zsh/zshrc.ubuntu
