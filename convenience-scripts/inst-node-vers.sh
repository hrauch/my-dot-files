#! /usr/bin/env bash

# perform a fresh install (all NodeJS LTS versions, NPM updates, and global pkgs)
# needs nvm (node version manager)

# Sanitize tty (in order to avoid staircase effect)
stty sane

echo "Checking whether nvm is present..."
if [ ! -d $HOME/.nvm ]; then
  echo -n "  No => trying to obtain it from github..."
  git clone https://github.com/creationix/nvm .nvm
  echo "done."
else
  echo -n "  Yes => trying to update it..."
  cd $HOME/.nvm && git pull && cd -
  echo "done."
fi

echo -n "Making nvm command available..."
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
echo "done."

# As there are no binary pkgs for FreeBSD, NodeJS needs to be compiled
# from src (I noticed compilation errors with the default clang/clang++
# (LLVM 6.0) compilers, thus revert to the 5.0 versions of both clang/clang++
if [ $(uname -s) = 'FreeBSD' ]; then
  export CC=clang50 CXX=clang++50
fi

install-global-npm-pkgs ()
{
  cd ~/gitlab/my-dot-files/convenience-scripts
  for i in $(cat npm-glbl-pkgs.lst); do
    npm i -g $i
    #yarn global add $i
  done
  cd -
}

cd ~/.nvm
echo -n "removing previously installed versions..."
rm -rf versions
echo "done."
echo -n "removing old nvm cache..."
rm -rf .cache
echo "done."
cd -
nvm install lts/argon
nvm use --delete-prefix lts/argon
# latest supported NPM version with Argon (4.x)
npm i -g npm@5.8.0
nvm install lts/boron
nvm use --delete-prefix lts/boron
npm i -g npm
install-global-npm-pkgs
nvm install lts/carbon
nvm use --delete-prefix lts/carbon
npm i -g npm
install-global-npm-pkgs
echo "Determining latest available NodeJS version..."
LATEST_NODEJS=$(nvm ls-remote | tail -1 | sed 's/^  *//')
echo "done."
nvm install $LATEST_NODEJS
nvm alias latest $LATEST_NODEJS
nvm use --delete-prefix latest
npm i -g npm
install-global-npm-pkgs
