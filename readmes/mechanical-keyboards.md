# X11 keyboard setup

https://wiki.archlinux.org/index.php/Xmodmap
https://wiki.archlinux.org/index.php/Xorg/Keyboard_configuration#Setting_keyboard_layout
https://wiki.archlinux.org/index.php/X_keyboard_extension#Using_rules
https://www.x.org/wiki/XKB/
https://www.x.org/releases/current/doc/xorg-docs/input/XKB-Config.html
https://who-t.blogspot.com/2008/09/rmlvo-keyboard-configuration.html
https://michal.kosmulski.org/computing/articles/custom-keyboard-layouts-xkb.html

https://www.freedesktop.org/wiki/Software/XKeyboardConfig/

General rule of thumb: Look around in

/usr/share/X11/xkb

https://en.wikipedia.org/wiki/Mouse_keys
https://wiki.archlinux.org/index.php/X_keyboard_extension#Mouse_control

===================

# Mechanical keyboards from here on

## Forums, Blogs, and Such

http://www.keyboard-layout-editor.com

http://xahlee.info/kbd/keyboard_blog.html
https://www.reddit.com/r/MechanicalKeyboards/comments/4ypstr/unicomp_buckling_spring_vs_matias_click_alps/
https://www.mechanical-keyboard.org
https://www.vergleich.org/mechanische-tastatur/
http://mechanische-tastatur.de/
https://www.keychatter.com/
https://www.keebtalk.com/
https://www.kiibohd.com
https://deskthority.net/haata-u71/
https://deskthority.net/wiki/Main_Page
https://en.wikipedia.org/wiki/List_of_mechanical_keyboards

======================================

Generally, keyboards having hot swappable switches preferred, as we can get by
w/o soldering (switches like e.g. Razer purple, Gateron optical, Wooting)

## General Keyboard Comparisons/Reviews in Dedicated Articles, But Outside Of
Forums

https://graphicscardhub.com/60-mechanical-keyboard/

https://www.anandtech.com/Show/Index/11150?cPage=1&all=False&sort=0&page=2&slug=tesoro-excalibur-se-spectrum-review-gateron-optical-switches

https://www.shopzadaph.com/2019/02/anne-pro-2-mechanical-keyboard-review.html

## Keyboard Stores

Good German store (IMHO): https://candykeys.com

https://mechanicalkeyboards.com

https://www.kbparadise.com/

https://alpacakeyboards.com

https://maxkeyboard.com

https://keyboardco.com

http://www.keyboardcatalog.com/

https://caps-unlocked.com

https://www.1upkeyboards.com/

https://kono.store

https://www.pckeyboard.com/

https://dream.tex-design.com.tw

https://mykeyboard.eu/
(unfortunately, most interesting keyboards, keycap sets, ...
are out of stock *smh* ...)

## Keyboard/Keycap/Switch/Cable Modding

Braided cables: https://mechcables.com

Keycaps: https://pimpmykeyboard.com

DYI kits: https://kbdfans.com

http://www.maxkeyboard.com/max-keyboard-nighthawk-full-customize-backlit-mechanical-keyboard.html

Custom DYI (w/o soldering?): https://www.epathbuy.com/

Unfortunately, no Gateron mechanical optical, hot-swappable switches, but
fucking soldering, thus not suited for me:
https://sentraq.com/products/basic-60-diy-keyboard-kit

Same soldering "shit":
https://input.club/devices/infinity-keyboard/infinity-60-build-guide/

## Keyboard Brands Supporting Linux via Ported SW

Keyboard Paradise (the 'R' variants)
Obins
Ergodox
Razer
Wooting
Vortex

### Fully FOSS (e.g. via QMK/TMK; Implies Hackability ;-) )

#### QMK/TMK

QMK firmware: https://beta.docs.qmk.fm/readme / qmk.fm
QMK configurator: https://config.qmk.fm
https://qmk.fmttps://github.com/qmk/qmk_firmware/tree/master/keyboards

http://blog.komar.be/projects/gh60-programmable-keyboard/
https://www.1upkeyboards.com/shop/controllers/gh60-satan-pcb/
https://www.1upkeyboards.com/shop/keyboard-kits/assembled-keyboards/green-envy-tkl-keyboard

https://shop.alpacakeyboards.com/products/hot-dox-complete-kit

TMK: https://github.com/tmk/tmk_keyboard
https://github.com/tmk/tmk_keyboard/wiki/TMK-Based-Projects

TMK Alps64: https://geekhack.org/index.php?topic=69666.0
TMK keymap editor: http://www.tmk-kbd.com/tmk_keyboard/editor/unimap/?alps64

https://clueboard.co/
https://atreus.technomancy.us/

https://www.ergodox.io/
https://ergodox-ez.com/
https://configure.ergodox-ez.com/
https://ergodox-ez.com/pages/satellite
https://ergodox-ez.com/pages/planck

https://keyboard.io

https://ultimatehackingkeyboard.com/
=> IMHO way too expensive w/ Kalih (instead of original
Cherry MX) switches

PFU distributes HHKB (Happy Hacking Keyboard) and
certain Topre Realforce keyboards (its EMEA subdivision
distributes at least some of them also in Europe)

https://www.pfu.fujitsu.com/en/
https://www.hhkeyboard.com/de  

Possibly cheaper alternatives to HHKB:

KB Paradise V60R w/ Matias Quiet Click switches
Allegedly programmable w/ open source FW:
µ controller: TMK ATMEGA32u4
https://github.com/topics/atmega32u4
https://deskthority.net/viewtopic.php?f=7&t=20112&start=
https://www.kbparadise.com/store/products/254608
=> $99

https://candykeys.com/product/kbparadise-v60-type-r
=> 125€ (but Cherry MX instead of Matias switches)

https://candykeys.com/product/vortex-cypher-split-spacebar
=> 99€

https://candykeys.com/product/vortex-race-3-ansi
=> 149€

Varmilo keyboards: https://www.varmilo.com/keyboardproscenium/en_
https://candykeys.com/product/va87m-cmyk-ansi
=> 155€

Vortex keyboards: http://vortexgear.tw
https://candykeys.com/product/vortex-pok3r-le-ansi
https://www.myeverydaytech.com/review-vortex-pok3r-rgb-mechanical-keyboard/
https://github.com/pok3r-custom/
=> Programmable via key combos, SW obviously only needed for firmware update

https://candykeys.com/product/tada68
=> 92€ (programmable)

#### Wooting (Wootility)

Flaretech switches are analog, hot swappable switches
=> IMHO worth giving a try

https://wooting.helpscoutdocs.com
https://wooting.store/products/wooting-two?variant=12273263509615
https://wooting.io/wootility
https://dev.wooting.io/
https://github.com/wootingkb/

https://www.amazon.de/dp/B07772B3HX?th=1
https://candykeys.com/product/wooting-one

### Obins (Obinskit)

http://en.obins.net/anne-pro2/
http://en.obins.net/obinskit

### At Least Partly (e.g. openrazer)

https://www.razer.com/gaming-keyboards-keypads/razer-huntsman-elite
https://www.razer.com/razer-opto-mechanical-switch
https://support.razer.com/gaming-keyboards/razer-huntsman-elite/
https://dl.razer.com/master-guides/RazerSynapse3/HuntsmanElite-00000550-en.pdf

https://openrazer.github.io/

What's not supported (yet) w/ openrazer:
* individual, per key RGB backlight (instead of constant color for all keys
  or effects)
* Swapping keys (e.g. left ctrl and caps lock) programmatically

Unfortunately, Windows only SW (as of yet): https://www.razer.com/synapse-3
Dto.: firmware updater tool: http://rzr.to/kDIkZ

## Programmable Keyboards, But Unfortunately Based on Win SW

### Topre Electro Capacitive Switches (Tactile, But Quiet)

http://www.realforce.co.jp/en/products/R2TLA-US4G-BK/index.html

Niz Atom 66
Niz Atom 84

Not bad, but can't compete w/ Razer Huntsman Elite
https://drevo.net/product/keyboard/tyrfing-v2
https://www.drevo.net/dpc

https://tesorotec.com/project/herpe-tl-mechanical-keyboard-with-trackball/
http://tesorotec.com/project/excalibur-se-spectrum-mechanical-gaming-keyboard/


## Non Programmable Ones, But At Least Ones Having DIP Switches

The "non R" variants of KBP (Keyboard Paradise)

## Non Programmable, No DIP Switches, But Still Interesting

Good vertical layout, does it count as being mechanical? http://typematrix.com/2030

Keychron K2 Aluminum RGB LED TKL Mechanical Keyboard                                                              
Glorious PC GMMK Prebuilt - TKL RGB LED Double Shot ABS Mechanical Keyboard

https://www.keychron.com/products/keychron-k2-mechanical-keyboard?variant=21572850745433 

http://matias.ca/switches/quiet/
https://matias.store/collections/mechanical-keyboards/products/quiet-pro-keyboard-for-pc

## Interesting Projects

https://shop.keyboard.io/
https://github.com/keyboardio

Keystone: https://input.club/
https://kono.store/products/keystone-analog-mechanical-keyboard
https://www.kickstarter.com/projects/lekashman/keystone-the-future-of-mechanical-keyboards

# Still Unsorted

https://www.getdigital.de/Matias-Ergo-Pro-Keyboard.html
https://www.getdigital.de/Nanoxia-Ncore-Retro-Mechanische-Tastatur.html
https://www.getdigital.de/Code-Keyboard-V2B.html

https://www.amazon.com/gp/offer-listing/B07MC7LLTR/ref=dp_olp_new_mbc?ie=UTF8&condition=new

https://mechanicalkeyboards.com/shop/index.php?l=product_list&c=12


Gateron optical switches (cheaper alternative to Topre ones):

https://flashquark.com/product/igk61-60-keyboard-with-gateron-optical-switches-and-usb-c/
https://www.epathbuy.com/product/gateron-optical-switches/
