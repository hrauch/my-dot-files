This is a collection of links to (mostly) open source stuff that I consider
useful. The following categorization is by no means to be regarded as "formally
correct", but solely my own way of categorizing various resources.

What's important to me?

* Ergonomics (meaning legible fonts and dark/black color themes)
* Software should be
  * available across different platforms
  * open source whenever possible (and reasonable in terms of features)

(The list below will continuously be updated and suggestions are welcome)

Except for Ergonomics the order is mostly arbitrary

## Ergonomics

Configurability with regards to dark/black color themes and choosing larger fonts for
menus/dialogs and editor windows is far more important to me (readability!) than
consistency solely for the sake of a "nice, clean look"

### Color Themes

* [Color Themes](http://color-themes.com) (for IDEs by Jetbrains)
* [oomox](https://github.com/themix-project/oomox)
* [GTK Vertex Themes](https://github.com/horst3180/Vertex-theme)
  (If anybody knows of a version for Qt, please notify me)

* [Slant: Best Dark Color Themes for Text Editors](https://www.slant.co/topics/358/~best-color-themes-for-text-editors)

### Browser Addons/Plugins Related to Color Themes

* [Dark Reader Plugin for Chrome/Chromium](https://chrome.google.com/webstore/detail/dark-reader/eimadpbcbfnmbkopoojfekhnkhdbieeh)
* [Morpheon Dark Theme for Chrome/Chromium](https://chrome.google.com/webstore/detail/morpheon-dark/mafbdhjdkjnoafhfelkjpchpaepjknad)
* [Owl Addon for Firefox](https://addons.mozilla.org/en-US/firefox/addon/owl/)

### Fonts

* [Typopro](http://typopro.org/)
* [Slant: Best Programming Fonts](https://www.slant.co/search?query=programming%20fonts)
* [Resource for Monospaced Coding Fonts](https://programmingfonts.org/)

* [Comme Font](https://github.com/vernnobile/commeFont) (fork of Oxygen)
* [IBM Plex](https://www.ibm.com/plex)
* [IBM Plex Repository](https://github.com/IBM/plex/)
* [Monoid Font](https://larsenwork.com/monoid/)
* [Fantasque Sans Mono Font](https://github.com/belluzj/fantasque-sans)
* [Bront Fonts](https://github.com/chrismwendt/bront)

Other IMHO good choices:
Borg Sans Mono if only it had a rounded ell
FiraCode if only it had a slashed zero

* [Paratype Public Fonts Sans,Serif,Mono](https://company.paratype.com/pt-sans-pt-serif)
* [Work Sans](http://weiweihuanghuang.github.io/Work-Sans/)

#### LaTeX Fonts

* [LaTeX Font Catalog](https://tug.dk/FontCatalogue)

**TODO:** real markdown links

https://tug.dk/FontCatalogue/cabin/                                             
small caps also: https://tug.dk/FontCatalogue/cantarell/                        
https://tug.dk/FontCatalogue/firasansbook/                                      
math: https://tug.dk/FontCatalogue/firasansmath/                                
math (alt.): https://tug.dk/FontCatalogue/firasansnewtxsf/                      
mono: https://tug.dk/FontCatalogue/firamono/                                    
https://tug.dk/FontCatalogue/gosans/                                            
mono: https://tug.dk/FontCatalogue/gomono/                                      
https://tug.dk/FontCatalogue/ibmplexsansregular/                                
https://tug.dk/FontCatalogue/ibmplexmonoregular/                                
https://tug.dk/FontCatalogue/inriasansregular/                                  
math support: https://tug.dk/FontCatalogue/iwona/                               
small caps & math: https://tug.dk/FontCatalogue/kpsansserif/                    
mono: https://tug.dk/FontCatalogue/kpmonospaced/                                
https://tug.dk/FontCatalogue/kurier/                                            
https://tug.dk/FontCatalogue/mintspirit/                                        
small caps available via caption variant:                                       
+https://tug.dk/FontCatalogue/paratypesans/                                     
https://tug.dk/FontCatalogue/raleway/

Monospaced (aka "typewriter") fonts:                                            
                                                                                
https://tug.dk/FontCatalogue/beramono/                                          
https://tug.dk/FontCatalogue/firamono/ (Fira (Sans) Code?)                      
https://tug.dk/FontCatalogue/gomono/                                            
https://tug.dk/FontCatalogue/ibmplexmonomedium/                                 
https://tug.dk/FontCatalogue/kpmonospaced/

### Editing

#### Distraction Free

* [Focuswriter](https://gottcode.org/focuswriter/)
* [Ghostwriter](https://github.com/wereturtle/ghostwriter)
* [Dark Room](http://jjafuller.com/dark-room)

#### Markdown

* [Caret](https://caret.io/)
* [Inkdrop](https://inkdrop.app/)
* [Joplin](https://joplinapp.org/)
* [Manuskript](http://www.theologeek.ch/manuskript/)
* [Notepadqq](https://notepadqq.com/wp/)
* [PileMd](https://pilemd.com/)
* [ReMarkable](https://remarkableapp.github.io/)
* [Typora](https://www.typora.io/)
* [Zettlr](https://www.zettlr.com/)

#### IDEs

* [JetBrains IntelliJ IDEA](https://www.jetbrains.com/idea/)
* [Sublime as Python IDE (Anaconda)](https://damnwidget.github.io/anaconda/)
* [Sublime Anaconda Repo](https://github.com/DamnWidget/anaconda)

#### GUI Text Editors

* [Sublime Text](https://www.sublimetext.com/)
* [TextAdept](https://foicica.com/textadept/)

* [Slick Edit](https://www.slickedit.com/)

* [Arachnophilia](https://arachnoid.com/arachnophilia/)
* [Cuda Text](http://uvviewsoft.com/cudatext/)

* [TextMate](https://macromates.com/)
  (Unfortunately, only available for mac OS, but has finally been open
  sourced in the meantime)
* [osxpeppermint](http://osxpeppermint.com/)
  (dto.)

### Android stuff

I use the following pkgs on all my phones (in conjunction with a dark
background that has still got enough contrast for the partly transparent
color gloss icons)

* [Smart Launcher](https://www.smartlauncher.net/)
  (use it with the classical "flower" design)
* [Color Gloss Icon Pack](https://android-apk.app/erega74.sltheme.colorgloss/)
* [Swift Installer](https://play.google.com/store/apps/details?id=com.brit.swiftinstaller&hl=en_US)
  (for black theming 3rd party apps, much more relaxing on eyes and even saves
  battery when used on devices with OLED screens)
* [Termux](https://termux.com/)
* [Aqua Mail](https://www.aqua-mail.com/)
* [Lightning](https://github.com/anthonycr/Lightning-Browser)
* [andOTP](https://github.com/andOTP/andOTP)
* [aCalendar+](https://play.google.com/store/apps/details?id=org.withouthat.acalendarplus&hl=en_US)
* [dmfs CalDAV Sync](https://dmfs.org/wiki/index.php?title=CalDAV-Sync)
* [dmfs CardDAV Sync](https://dmfs.org/wiki/index.php?title=CardDAV-Sync)
* [dmfs Contact Editor Pro](https://dmfs.org/wiki/index.php?title=Contact_Editor_Pro)
* [DW Contacts & Dialer Pro](http://blog.dw-p.net/)

### Misc

* [Editorconfig](https://editorconfig.org)
* [Editorconfig repositories](https://github.com/editorconfig/)
  (common indentation style across various IDEs, editors, ...)

* [Spacemacs](https://github.com/syl20bnr/spacemacs)
  (Basically a nice color theme; would like to have it inverted, however,
  i.e. fully black background and dark gray highlighted current line and
  choose larger fonts)

## General X11 And Wayland Related Stuff (Window Managers/Desktop Environments)

### X11

#### UI Toolkits

https://sourceforge.net/projects/motif/

#### Window Managers

* [Tabular Overview of X11 Window Managers](https://www.gilesorr.com/wm/table.html)

https://ice-wm.org/

https://github.com/fvwmorg/fvwm/
https://sourceforge.net/projects/fvwm-crystal/

http://openbox.org/wiki/Main_Page [not really updated since 3 years, but
still powerful]
https://www.box-look.org

https://github.com/ch11ng/exwm

http://herbstluftwm.org

https://github.com/conformal/spectrwm

https://sourceforge.net/projects/monkeywm/

#### Desktop Environments

https://www.enlightenment.org/
https://www.opendesktop.org/
https://sourceforge.net/projects/wizarddesktop/


https://sourceforge.net/projects/cdesktopenv/
https://sourceforge.net/p/cdesktopenv/wiki/Home/
https://sourceforge.net/p/cdesktopenv/wiki/SupportedPlatforms/
https://sourceforge.net/p/cdesktopenv/wiki/LinuxBuild/
https://sourceforge.net/p/cdesktopenv/wiki/Archlinux_Build/

### Wayland

https://wayland.freedesktop.org/
https://mir-server.io
http://way-cooler.org/
https://swaywm.org/

### Misc (As of Yet Unsorted)

**TODO:** real markdown links

https://github.com/bbidulock/icewm

https://github.com/bbidulock/adwm

https://github.com/bbidulock/xde-menu/


http://byobu.co/

http://pierotofy.github.io/glassomium/

http://dwm.suckless.org/


https://github.com/baskerville/bspwm
https://github.com/baskerville/sxhkd

## Console Applications

These have got the main advantage of being able to use the same font
originally selected in the terminal

### Terminal Emulators

* [Terminals and True Color](https://gist.github.com/XVilka/8346728)

* [sakura](http://troubleshooters.com/linux/sakura.htm)
* [Termite](https://github.com/thestinger/termite)

* [vte-ng](https://github.com/thestinger/vte-ng)
  **TODO:** Compare to **current** libvte

### Terminal Multiplexers

#### tmux

* [tmux](https://github.com/tmux/tmux/wiki)
* [tmux FAQ](https://github.com/tmux/tmux/wiki/FAQ)
* [Practical tmux](http://mutelight.org/practical-tmux)
* [Book Tao of Tmux](https://leanpub.com/the-tao-of-tmux/read)
* [tmux Cheat Sheet](https://tmuxcheatsheet.com/)
* [tmux on ArchLinux](https://wiki.archlinux.org/index.php/Tmux)

#### tmux Alternatives

* [pymux](https://github.com/prompt-toolkit/pymux)
* [abduco](http://www.brain-dump.org/projects/abduco/)
* [mtm](https://github.com/deadpixi/mtm)
* [TWin](https://github.com/cosmos72/twin/)

### Text Editors

(**E**ight **M**egabytes **A**lmost **C**ontinuously **S**wapping ;-) )

Apart from that, I've never really liked the distinction between modes;
when I open an editor, I want to proceed to enter text straight away.
Period. That's why I don't mention vi and its clones ;-) (Yes, I'm aware
vi can be configured to start off right in append/insert mode, but one
still can't get around that madning mode concept...

* [Emacs](https://www.gnu.org/software/emacs/)
* [jed](http://www.jedsoft.org/jed/)
* [mg](https://github.com/hboetes/mg/)

I've had my first text processing experience with a program called WordStar,
back in 1990, so I'm still sort of used to the key combos ;-) That's why Joe
is mentioned here:

* [Joe](https://joe-editor.sourceforge.io/)
* [Jupp](https://www.mirbsd.org/jupp.htm)

* [Nano](https://www.nano-editor.org/)

* [dte](https://craigbarnes.gitlab.io/dte/)

### Mail

* [Neomutt](https://github.com/neomutt)
* [Mutt](https://gitlab.com/muttmua/mutt)
* [Alpine](https://repo.or.cz/alpine.git)
* [Mew (for Emacs)](http://www.mew.org/en/)

### Web Browsers

* [W3M](http://w3m.sourceforge.net/index.en.html)
  (use it inside Neomutt to have those HTML mails converted to plain text)

### File Manager

* [ytree](https://www.han.de/~werner/ytree.html)
* [Midnight Commander](https://midnight-commander.org/)

### IRC

* [irssi](https://irssi.org/)
* [irssi documentation](https://irssi.org/documentation/)
* [irssi on ArchLinux](https://wiki.archlinux.org/index.php/Irssi)
* [Correct Use of tmux and irssi via ssh](https://superuser.com/questions/579867/correct-use-of-tmux-and-irssi-via-ssh)

* [Weechat](https://weechat.org/)
* [Weechat Scripting](https://weechat.org/files/doc/stable/weechat_scripting.en.html)
* [WeeChat ArchLinux](https://wiki.archlinux.org/index.php/WeeChat)
* [Weechat Notify Send](https://github.com/s3rvac/weechat-notify-send)
* [Pure Python OTR](https://github.com/python-otr/pure-python-otr)

* [tmux weechat quickstart](https://mikaela.info/blog/english/2016/03/09/weechat-tmux-quickstart.html)
* [tmux-env.py](https://weechat.org/scripts/source/tmux_env.py.html/)
* [weechat tmux env](https://github.com/agriffis/weechat-tmux-env)
* [notify.py](https://weechat.org/scripts/source/notify.py.html/)
* [screen away](https://weechat.org/scripts/source/screen_away.py.html/)

### Misc

* [moc](https://moc.daper.net/)
* [Snownews](https://github.com/kouya/snownews)

* [termbox](https://github.com/nsf/termbox) (ncurses alternative)

* [mosh](https://mosh.org/)

## Revision Control Systems (RCS) And Solutions Based Upon Them

* [Sourcehut](https://sourcehut.org)
* [Sourcehut Manual(s)](https://man.sr.ht/)
* [Sourcehut Service Documentation](https://man.sr.ht/#service-documentation)
* [Sourcehut Installation](https://man.sr.ht/installation.md)
* [Pkgs for Alpine, Arch, Debian Unstable aka Sid](https://mirror.sr.ht/)
* [Compatibility Matrix](https://man.sr.ht/builds.sr.ht/compatibility.md)

Even though Sourcehut is still in an early stage, it looks promising, as many
features you'd normally expect only in solutions such as Github/Gitlab are
already included:
* Mercurial (hg) or git as RCS
* Wiki (based on markdown and the underlying RCS chosen for a particular project)
* Task/issue mgmt
* User account mgmt
* Continuous Integration (CI)
* Maling lists (can even send encrypted mails)
* Code review tools

Open Items:

* macOS
* iOS
* Windows

available for CI builds?

Advantages (IMHO):

* No 'language mix' (just Python AFAIK)
* 'Reasonable' dependencies (PostgreSQL, Redis, cron daemon,
  mail server (MTA), HTTP server (Nginx, Apache), Python (some modules req'd);
  see [Arch PKGBUILD file](https://git.sr.ht/~sircmpwn/sr.ht-pkgbuilds/tree/master/python-srht/PKGBUILD) for details)
* Liberal open source license (see [LICENSE](https://git.sr.ht/~sircmpwn/core.sr.ht/tree/master/LICENSE)
  for details)


## Books

* [Pragmatic Bookshelf](https://pragprog.com/categories/all)

## Cross Platform Instant Messaging/VoIP/Video Conferencing

* [Jitsi](https://jitsi.org/)
* [Matrix](https://matrix.org)
* [Riot](https://riot.im) (based on Matrix, see above)
  (Available for Android, iOS, web, desktop, supports encryption)

* [Mumble](https://wiki.mumble.info/wiki/Main_Page)

## Build Tools

Why didn't I mention Ant and Maven here? Because XML is IMHO too verbose for
declarative programming and thus too awkward to read.

* [Useful Overview on Build Tools](https://github.com/ninja-build/ninja/wiki/List-of-generators-producing-ninja-build-files)

* [Bazel](https://bazel.build/)
* [CMake](https://cmake.org) (alternative to GNU autotools)
* [Gradle](https://gradle.org)
* [gn](https://gn.googlesource.com/gn/)
* [Ninja](https://ninja-build.org/)
* [Meson](https://mesonbuild.com)
* [waf](https://waf.io/)
* [waf book](https://waf.io/book/)
* [waf examples](https://gitlab.com/ita1024/waf/tree/master/demos)
* [waf repository](https://gitlab.com/ita1024/waf)

* [autosetup](http://msteveb.github.io/autosetup/)

## Web Browsers And Web Browser Features

* [Can I Use](https://caniuse.com/)
* [Chromium](https://github.com/chromium/chromium)
* [Chrome Devtools](https://developers.google.com/web/tools/chrome-devtools/)

## Crypto / OpenPGP

* [sequoia-pgp](https://sequoia-pgp.org/)
* [Nettle](https://sequoia-pgp.gitlab.io/nettle-rs/nettle/)
* [ring](https://github.com/briansmith/ring)
* [rust-crypto](https://github.com/DaGenix/rust-crypto/)
* [tomcrypt-rs](https://github.com/ReSpeak/tomcrypt-rs)

* [libsodium](https://download.libsodium.org/doc/)
* [NaCl](http://nacl.cr.yp.to/)

## Architectural Patterns

* [Consumer-Driven Contracts](https://www.martinfowler.com/articles/feature-toggles.html)
* [Design Patterns in Java Tutorial](https://www.tutorialspoint.com/design_pattern/)
* [Feature Toggles](https://www.martinfowler.com/articles/feature-toggles.html)
* [GoF Patterns](https://www.gofpatterns.com/)
* [Microservices](https://www.martinfowler.com/articles/microservices.html)

## Programming Languages

### Concepts

Like mixins, traits, ...

* [Mixins](https://en.wikipedia.org/wiki/Mixin)
* [Traits](https://en.wikipedia.org/wiki/Trait_(computer_programming))

### Generators

* [Simple Wrapper And Interface Generator SWIG](https://swig.org)

### Ada

* [AdaCore](https://adacore.com)
* [Ada Information Clearinghouse](https://adaic.org)
* [ParaSail](https://parasail-lang.org)

### Chapel

* [Chapel](https://chapel-lang.org)

### Crystal

* [Crystal](https://crystal-lang.org/)
* [Crystal Reference](https://crystal-lang.org/reference/)

(Ruby without TIMTOWTDI?)

### D

* [D](https://dlang.org)
* [LDC2](https://github.com/ldc-developers/ldc)
* [DUB Package Manager](https://code.dlang.org/packages/dub)
* [DUB Packages](https://code.dlang.org/)

* [Tilix](https://github.com/gnunn1/tilix)
* [GtkD](https://github.com/gtkd-developers/GtkD)

### Dart

* [Dart](https://dart-lang.org]

(Possible alternative to TypeScript? Has anybody come across a **current**
comparison of these two languages?)

### Erlang

* [Erlang](https://www.erlang.org/)
* [Erlang Programming Rules and Conventions](http://www.erlang.se/doc/programming_rules.shtml)
* [Erlang Central](https://erlangcentral.org/)
* [Hex Package Manager](https://hex.pm/) (for both Erlang and Elixir)

### Elixir

* [Elixir](https://elixir-lang.org/)

### Elm

* [Elm](https://elm-lang.org/)

(Possible alternative to TypeScript?)

### Falcon2

* [Falcon2](https://github.com/falconpl/Falcon2)

### Groovy

* [Groovy](https://groovy-lang.org)

### Haskell

* [Haskell](https://www.haskell.org/)
* [Haskell Wiki](https://wiki.haskell.org/Haskell)
* [GHC Glasgow Haskell Compiler](https://www.haskell.org/ghc/)
* [Haskell Platform](https://www.haskell.org/platform/)
* [Haskell Tool Stack](https://docs.haskellstack.org/en/stable/README/)
* [Cabal Build System](https://www.haskell.org/cabal/)
* [Hackage - Haskell Packages](http://hackage.haskell.org/)

### Hy

* [Hy](http://docs.hylang.org/en/stable/)

LISP combined with Python features)

### Kotlin

* [Kotlin](https://kotlinlang.org/)

### Java/JEE

#### Misc

* [OpenJDK Wiki](https://wiki.openjdk.java.net/)
* [Spring Boot](https://docs.spring.io/spring-boot/docs/current/reference/html/)
* [Eclipse Microprofile](https://microprofile.io/)
* [OpenLiberty](https://openliberty.io/)
* [Payara](https://payara.fish)
* [Thorntail](https://thorntail.io/)
* [Wildfly](https://www.wildfly.org/)
* [Java EE Doc](https://javaee.github.io/)
* [Java EE Repositories](https://github.com/javaee)
* [Glassfish](https://javaee.github.io/glassfish/documentation)
* [Java EE 8 Spec](https://javaee.github.io/javaee-spec/)
* [EE4J Repositories](https://github.com/eclipse-ee4j/)
* [EE4J Documentation](https://projects.eclipse.org/projects/ee4j)
* [Eclipse EE4J OpenMQ](https://github.com/eclipse-ee4j/openmq)
* [Apache ActiveMQ](https://activemq.apache.org/)
* [TomEE](http://tomee.apache.org/)
* [Jersey](https://jersey.github.io/)
* [Netty](https://netty.io/)

* [WebSwing](http://webswing.org/)

* [Design Patterns in Java](https://www.tutorialspoint.com/design_pattern)
  (Design patterns as such are language agnostic)

* [Application Class Data Sharing](https://openjdk.java.net/jeps/310)

* [Java Native Access](https://github.com/java-native-access/jna)
* [Java Native Access Native Libs](https://github.com/java-native-access/jna/tree/master/lib/native)
* [Java Native Runtime](https://github.com/jnr)

* [Oracle Community: Lambda Usage With Collections And Method References](https://community.oracle.com/docs/DOC-1006954)
* [Oracle Community: JavaEE & Docker](https://community.oracle.com/docs/DOC-1008824)

#### Java Interacting w/ Native Languages

##### JNI

* [Official JNI Spec](https://docs.oracle.com/en/java/javase/12/docs/specs/jni/index.html)
* [JNI Tips (not only for Android)](https://developer.android.com/training/articles/perf-jni)
* [jni-rs (Rust Crate)](https://github.com/jni-rs/jni-rs)

##### JNI-'less' (w/o JNI)

* [Project Panama](https://openjdk.java.net/projects/panama/)

* [GraalVM](https://www.graalvm.org/)
* [GraalVM: Why?](https://www.graalvm.org/docs/why-graal/)
* [GraalVM: Examples](https://www.graalvm.org/docs/examples/)
* [GraalVM: Top Ten Things](https://medium.com/graalvm/graalvm-ten-things-12d9111f307d)

### Lua

* [Lua](https://lua.org)

### **Cross Platform** .NET (.NET Core/Mono)

* [.NET Core Repository](https://github.com/dotnet/core)
* [.NET Core Documentation](https://docs.microsoft.com/en-us/dotnet/core/)
* [Powershell Core Repository](https://github.com/powershell/powershell)
* [Powershell Core Documentation](https://docs.microsoft.com/en-us/powershell)
* [Mono](https://mono-project.com)

### Nim

* [Nim](https://nim-lang.org)

### Ocaml

* [Ocaml](https://ocaml.org/)
* [Opam Package Manager](https://opam.ocaml.org/)

### Python

* [Python](https://python.org)
* [PyPi](https://pypi.org)
* [Poetry](https://poetry.eustace.io/)
* [Dynaconf](https://dynaconf.readthedocs.io/en/latest/)

* [Prompt Toolkit](https://github.com/prompt-toolkit)
* [Pygments](http://pygments.org/)

* [Alembic (Based on SQLAlchemy)](https://alembic.sqlalchemy.org/en/latest/)

### Ring

* [Ring](https://ring-lang.org)

### Ruby

* [Ruby](https://ruby-lang.org)
* [Ruby Gems](https://rubygems.org)

### Rust

* [Rust](https://rust-lang.org)
* [Rust Release Notes](https://github.com/rust-lang/rust/blob/master/RELEASES.md)
* [Rust Blog](https://blog.rust-lang.org/)
* [Crates](https://crates.io/)

* [Awesome Rust](https://github.com/mmstick/awesome-rust)

* [Redox OS](https://www.redox-os.org)

* [resvg](https://github.com/RazrFalcon/resvg)

* [core utils](https://github.com/uutils/coreutils)
* [find utils](https://github.com/uutils/findutils)

* [remacs](https://github.com/remacs/remacs)

* [tectonic](https://github.com/tectonic-typesetting/tectonic)

* [gtk-rs](https://gtk-rs.org/)
* [gtk-rs repository](https://github.com/gtk-rs/gtk)
* [gtk-rs examples repository](https://github.com/gtk-rs/examples)

* [Way Cooler Tiling WM](http://way-cooler.org/)

* [systemd-manager](https://github.com/mmstick/systemd-manager)

* [Pijul VCS](https://pijul.org)

* [mdbook](https://rust-lang-nursery.github.io/mdBook/)

* [jni-rs](https://github.com/jni-rs/jni-rs)

* [sequoia-pgp](https://sequoia-pgp.org/)
* [Nettle](https://sequoia-pgp.gitlab.io/nettle-rs/nettle/)
* [ring](https://github.com/briansmith/ring)
* [rust-crypto](https://github.com/DaGenix/rust-crypto/)
* [tomcrypt-rs](https://github.com/ReSpeak/tomcrypt-rs)

* [LD_PRELOAD & rustc segfault bug 1](https://github.com/rust-lang/rust/issues/59032)
* [LD_PRELOAD & rustc segfault bug 2](https://github.com/rust-lang/rust/issues/56736)

### Sass (Syntactically Awesome Stylesheets)

* [Sass](https://sass-lang.com/)
* [Sass Lint](https://github.com/sasstools/sass-lint)

### Scala

* [Scala](https://scala-lang.org)

### Squirrel

* [Squirrel](http://squirrel-lang.org/)
* [Squirrel Wiki](http://wiki.squirrel-lang.org/mainsite/Wiki/default.aspx/SquirrelWiki/Home.html)

### Swift

* [Swift](https://swift.org)

### Tcl

* [Jim Tcl](http://jim.tcl.tk/index.html/doc/www/www/index.html)

### TOML

* [Toml](https://github.com/toml-lang/toml)
* [Toml Wiki](https://github.com/toml-lang/toml/wiki)

### JavaScript Packaging/Node.js/TypeScript

* [Yarn](https://yarnpkg.com)
* [TypeScript](http://www.typescriptlang.org/)
* [TypeScript Book](ttps://github.com/basarat/typescript-book)
* [TypeScript Collections Framework](https://github.com/larrydiamond/typescriptcollectionsframework)
* [TSLint](https://palantir.github.io/tslint/)
* [Webpack](https://webpack.js.org/)
* [Brunch](https://brunch.io/)

### XSLT3

* [General Feature Overview](https://www.xml.com/articles/2017/02/14/why-you-should-be-using-xslt-30/) 
* [Spec](https://www.w3.org/TR/xslt-30/)

### ZSH And (Some) Bash

* [zsh](http://www.zsh.org/)
* [Zsh Lovers](http://www.guckes.net/zsh/lover.html)
* [Oh My Zsh](https://ohmyz.sh/)
* [Oh My Zsh Repository](https://github.com/robbyrussell/oh-my-zsh)
* [Grml Zsh](https://grml.org/zsh/)
* [Grml Zsh Resources](https://grml.org/zsh/#resources)

* [Bash Automated Testing System BATS](https://github.com/sstephenson/bats)
* [Shellcheck](https://www.shellcheck.net/)

## API Mgmt And Documentation

* [Swagger](https://swagger.io/)
* [OpenAPIs](https://www.openapis.org/)

## General Web Frontend Patterns

* [BEM](https://en.bem.info)

## UI Frameworks

### JavaScript [Preferrably TypeScript)

* [Angular](https://angular.io)
* [Aurelia](https://aurelia.io)
* [Dojo](https://dojo.io)
* [Electron](https://electronjs.org/) (probably based on)
* [Chromium Embedded Framework](https://bitbucket.org/chromiumembedded/cef)
* [Gatsby](https://www.gatsbyjs.org/)
* [Vue](https://vuejs.org)

### C (Originally)

* [Gtk Language Bindings](https://www.gtk.org/language-bindings.php)

### C++ (Originally)

* [Qt](https://www.qt.io/)
* [Qt Language Bindings](https://wiki.qt.io/Language_Bindings)

## Testing Frameworks and DSLs

### Consumer Driven Contract Testing

* [Article on Pact](https://blog.risingstack.com/consumer-driven-contract-testing-with-pact/
* [Pact](https://docs.pact.io/)
* [Pact Foundation](https://github.com/pact-foundation/)

### (Mostly) Language Agnostic

* [Cucumber](https://cucumber.io)
* [Geb](https://gebish.org)
* [RobotFramework](https://robotframework.org/)

### JavaScript/TypeScript

* [Appium](https://appium.io)
* [Cypress](https://cypress.io)
* [Enzyme](https://airbnb.io/enzyme/)
* [Jasmine](https://jasmine.github.io/)
* [Karma](https://karma-runner.github.io/latest/index.html)
* [Lighthouse](https://developers.google.com/web/tools/lighthouse/)
* [Mocha](https://mochajs.org/)
* [Protractor](https://www.protractortest.org/#/)
* [Puppeteer](https://pptr.dev/)
* [Tap](https://www.node-tap.org/)

### Java

* [JUnit](https://junit.org/junit5/)
* [TestNG](https://testng.org/doc/index.html)
* [Spock](http://spockframework.org/)

### Python

* [Tox](https://tox.readthedocs.io/en/latest/)

## Infrastructure as Code

* [Ansible](https://www.ansible.com/)
* [Chef](https://www.chef.io/chef/)
* [Puppet](https://puppet.com/#)
* [Saltstack aka Salt](https://docs.saltstack.com/en/latest/)
* [LCFG](https://wiki.lcfg.org/bin/view/LCFG)
* [Terraform](https://www.terraform.io/)

## CI (Continuous Integration)

* [84codes](https://www.84codes.com/)
* [Appveyor](https://www.appveyor.com/)
* [Circle CI](https://circleci.com/)
* [Cloudbees Jenkins](https://www.cloudbees.com/jenkins/about)
* [Go CD](https://www.gocd.org/)
* [TeamCity](https://www.jetbrains.com/teamcity/)
* [Travis CI](https://travis-ci.org/)
* [BuildBot](http://docs.buildbot.net/latest/manual/)

## DBs/Search Engines

* [Elasticsearch](https://www.elastic.co/products/elasticsearch)
* [Logstash](https://www.elastic.co/products/logstash)
* [ArangoDB](https://www.arangodb.com/)
* [Xapian](https://xapian.org/)

* [PostgreSQL](https://www.postgresql.org/)

* [BaseX](http://basex.org/)
* [eXist DB](http://www.exist-db.org)

## Virtualization/Containerization/Cloud

* [libvirt](https://libvirt.org/)
* [libvirt and BHyve](https://libvirt.org/drvbhyve.html)
* [libvirt and LXC](https://libvirt.org/drvlxc.html)
* [libvirt and QEMU](https://libvirt.org/drvqemu.html)
* [Eucalyptus](https://github.com/eucalyptus/eucalyptus)
* [Snooze](http://snooze.inria.fr/)
* [Cherrypop](https://github.com/gustavfranssonnyvell/cherrypop)
* [ZStack](http://en.zstack.io/)
* [Vagrant](https://www.vagrantup.com/)
* [Packer](https://packer.io)
* [Bento](https://github.com/chef/bento)

* [Open Container Intiative (OCI)](https://www.opencontainers.org/)
* [containerd](https://containerd.io)
* [Podman](https://podman.io)
* [Jujucharms](https://jujucharms.com/)
* [Kubernetes](https://k8s.io)
* [kompose](https://github.com/kubernetes-incubator/kompose)
* [Kubernetes/Kontena Pharos Cluster](https://github.com/kontena/pharos-cluster/)
* [Kontena](https://kontena.io)
* [Openstack](https://www.openstack.org/)
* [Openshift](https://openshift.io/)
* [Project Atomic](http://www.projectatomic.io/)
* [Buildah](https://github.com/containers/buildah)
* [Skopeo](https://github.com/containers/skopeo)
* [Mist](https://mist.io/)
* [Google Container Tools](https://github.com/GoogleContainerTools)
* [Istio](https://istio.io)
* [Moby Project](https://mobyproject.org/)
* [Moby Buildkit](https://github.com/moby/buildkit)
* [OpenFaaS Cloud](https://github.com/openfaas/openfaas-cloud)
* [img](https://github.com/genuinetools/img)
* [Genuinetools](ttps://genuinetools.org/)

## Screen Sharing

### NoMachine Aka NX

https://www.nomachine.com/                                                                                                    
https://www.nomachine.com/download                                                                                            
https://code.google.com/archive/p/neatx/                                                                                      
http://opennx.sourceforge.net/                                                                                                
https://en.wikipedia.org/wiki/NX_technology

### Mikogo (Browser Based)

https://www.mikogo.de/funktionen/                                                                                             
https://www.mikogo.de/downloads/docs/mikogo-sicherheit.pdf

### Remmina

https://remmina.org/#

### x2go

https://wiki.x2go.org/doku.php

## Screen Capturing/Recording

* [OBS Studio](https://obsproject.com/)
* [Simple Screen Recorder](https://www.maartenbaert.be/simplescreenrecorder/)
* [Vokoscreen](http://linuxecke.volkoh.de/vokoscreen/vokoscreen.html)

## Browser Based Video Conferencing

* [Jitsi](https://jitsi.org/)
* [Appear.in](https://appear.in/)
* [Talky](https://talky.io/)
* [GoToMeeting](https://free.gotomeeting.com/)

## Documentation/Diagramming Tools and Markup Languages

### Converters

* [MMark](https://mmark.nl/)
* [MMark Repository](https://github.com/mmarkdown/mmark)
* [Pandoc](https://pandoc.org)
* [Java ASCII Art Editor (Jave)](http://jave.de/)
* [Svgbob](https://ivanceras.github.io/svgbob-editor/)
* [Svgbob Repository](https://github.com/ivanceras/svgbob/)

### Wikis

* [Wiki Comparison Matrix Wizard](https://www.wikimatrix.org/wizard)

The following wikis use either git or mercurial as VCS, and - for the
sake of readability/maintainability - are **not** implemented in Perl,
PHP, C, C++:

* [Hatta Wiki](http://hatta-wiki.org/)
* [Oleo Wiki](https://github.com/minad/olelo)
* [Zim Wiki](https://zim-wiki.org)
* [Gitit Wiki](https://hackage.haskell.org/package/gitit)
(unfortunately, its web UI lacks features such as commenting articles,
breadcrumb navigation, easy renaming of articles). Apart from that,
I like the combo of having a CVS as backend and pandoc as converter for
exports (from markdown markup) to PDF, LaTeX, and many other formats.

### Groff/Troff And Friends (Keep The Flavor Of The Old Skool) ;-)

* [Overview on Troff](https://www.troff.org/)
* [Groff](http://git.savannah.gnu.org/git/groff)
* [Heirloom Documentation Tools](http://heirloom.sourceforge.net/doctools.html)
* [mom macros](http://www.schaffter.ca/mom/mom-01.html)
* [Plan9 Troff](git://repo.or.cz/troff.git)
* [utroff](https://utroff.org)

### Static Site Generators

* [Overview](https://www.staticgen.com/)
* [Middleman](https://middlemanapp.com/)
* [Middleman Repository](https://github.com/middleman/middleman)
* [Hugo](https://gohugo.io)
* [Pelican](https://blog.getpelican.com/)
* [Jekyll](https://jekyllrb.com/)
* [Nanoc](https://nanoc.ws/) 
* [Docosaurus](https://docusaurus.io/)
* [Yass](https://github.com/yet-another-static-site-generator/yass)
* [Dssg](https://github.com/kambrium/dssg)
* [Styx](https://github.com/styx-static/styx)
* [Cobalt](https://github.com/cobalt-org/cobalt.rs)
* [Stasis](https://github.com/Gioni06/stasis-generator)
* [Hastysite](https://github.com/h3rald/hastysite)
* [Stog](https://github.com/zoggy/stog)
* [Luapress](https://github.com/Fizzadar/Luapress)
* [Coleslaw](https://github.com/coleslaw-org/coleslaw)
* [Vuepress](https://github.com/vuejs/vuepress)

### Markdown

* [Markdown](https://gerrit.googlesource.com/gitiles/+/master/Documentation/markdown.md)
* [md_browser](https://github.com/chromium/chromium/tree/master/tools/md_browser)
* [Gitiles](https://gerrit.googlesource.com/gitiles/)
* [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
* [Gitbook](https://www.gitbook.com/)
* [Github Wiki => PDF](https://blog.terryburton.co.uk/2015/02/20/Creating-PDF-Documentation-From-a-GitHub-Wiki-Using-Pandoc.html)
* [kramdown](https://kramdown.gettalong.org/)
* [redcarpet](https://github.com/vmg/redcarpet)
* [Gitlab Markdown](https://gitlab.com/help/user/markdown.md)
* [Sphinx And Markdown Sample](https://github.com/juju-solutions/sphinx-markdown-sample)
* [mdbook](https://rust-lang-nursery.github.io/mdBook/)

### LaTeX

* [KOMAScript](https://komascript.de)
* [LaTeX Templates](https://latextemplates.com)
* [Speedata Publisher](https://www.speedata.de/en/)
* [TecTonic Typesetting](https://tectonic-typesetting.github.io/en-US/)

### Misc

* [AsciiDoctor](https://asciidoctor.org/) based on
* [AsciiDoc](https://asciidoc.org/)
* [Forestry](https://forestry.io/)
* [Sphinx](http://www.sphinx-doc.org/en/master/)
* [Ghost](https://ghost.org)
* [Tables Generator](https://www.tablesgenerator.com/)
* [Tesseract OCR](https://github.com/tesseract-ocr)
* [PP (Preprecessor with Pandoc In Mind)](http://cdsoft.fr/pp/)

### Diagramming

* [Draw.io Batch](https://github.com/languitar/drawio-batch)
* [Graphviz](https://www.graphviz.org/)
* [Lucidchart](https://www.lucidchart.com/)
* [PlantUML](http://plantuml.com/de)
* [asciiflow](http://asciiflow.com/)
* [Jave](http://www.jave.de)
* [mscgen (Message Sequence Chart Generator)](http://www.mcternan.me.uk/mscgen/)
* [ditaa (Diagrams Through ASCII Art)](http://ditaa.sourceforge.net/)
* [blockdiag](http://blockdiag.com/en/blockdiag/)
* [aafigure](https://aafigure.readthedocs.io/en/latest/manual.html)

### Documenting Source Code

#### Multiple Languages

* [Doxygen](https://doxygen.nl)

#### JavaScript/TypeScript

* [CompoDoc](https://compodoc.github.io/compodoc/)
* [TypeDoc](https://typedoc.org/)

## Linters Not Mentioned Elsewhere In This File

* [Commitlint](https://commitlint.com/)
* [ESLint](https://eslint.org/)
* [HtmlHint](https://htmlhint.io/)
* [Stylelint](https://stylelint.io/)
* [Xmllint - Part of libxml2](ftp://xmlsoft.org/libxml2/)

For C/C++ in particular:

* [PC-Lint Plus](https://gimpel.com/for_pclint_users.html) (the successor of
* FlexeLint (no longer maintained)

## Code Analysis (for Vulns and Such)

*NOTE:* Even though some of these are commercial, these tools can usually
be used for free (legally) when hosting *open source* projects

* [Codeclimate](https://docs.codeclimate.com/)
* [Codecov](https://codecov.io/)
* [Coveralls](https://coveralls.io/)
* [Snyk](https://snyk.io)
* [Coverity Scan](https://scan.coverity.com/)

(I sincerely hope you don't have to code in - or debug - "plain" C/C++,
but in case you do...)

* [Valgrind](http://www.valgrind.org/)

* [Insure++](https://www.parasoft.com/products/insure)
* [PurifyPlus](https://www.teamblue.unicomsi.com/products/purifyplus/)

## OS Stuff

### HardenedBSD

* [HardenendBSD](https://hardenedbsd.org)

### OpenBSD

* [OpenBSD](https://openbsd.org)

### Redox OS

* [Redox OS](https://redox-os.org/)

(An operating system written in Rust => finally a modern programming language
compared to C/C++))

### SmartOS

* [SmartOS](https://smartos.org)
* [SmartOS](https://www.joyent.com/smartos)
* [SmartOS Wiki](https://wiki.smartos.org/display/DOC/Home)

## Filesystem Stuff

### Distributed

* [Ceph](https://ceph.com/)
* [Tahoe LAFS](https://tahoe-lafs.org/trac/tahoe-lafs)

### Local

* [OpenZFS](http://open-zfs.org/wiki/Main_Page)
* [ZFS on Linux](https://zfsonlinux.org/)

## Network Stuff

### Protocol Related

#### NTP

* [NTPSec](https://www.ntpsec.org)
* [NTPSec Repository](https://gitlab.com/NTPsec/ntpsec)

### VPN

* [glorytun](https://github.com/angt/glorytun)
* [Wireguard](https://www.wireguard.com/)

### Decentralized

* [I2P](https://geti2p.net/en/)

## Security Tools

* [GauntLT](https://gauntlt.org)
* [OWASP ZAP](https://github.com/zaproxy/)

* [Wire](https://github.com/wireapp/wire)

## (Smart)Phone Security (Mostly Android Centric)

* [Authy (2FA solution)](https://authy.com/)
* [Psiphon](https://psiphon.ca)
* [Wire (Secure Messaging App w/o Phone No)](https://app.wire.com/)
* [Scrambled Exif App](https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif)
* [Jitsi Meet (Free, Encrypted Video Conferencing)](https://jitsi.org/jitsi-meet/)
* [Creating a Secure Online Persona](https://medium.com/@geminiimatt/creating-an-online-persona-deb4cd8c7f46)
* [Tutanota Mail Service](https://tutanota.com)

## IBM Mainframe (Misc Stuff, Unsorted)

### z/OS

* [Main Page](https://www.ibm.com/it-infrastructure/z/zos)
* [JDK 8 for z/OS](https://developer.ibm.com/javasdk/support/zos/#v8)
* [NodeJS](https://www.ibm.com/de-de/marketplace/sdk-nodejs-compiler-zos)
* [XML System Services Functions](https://www.ibm.com/support/knowledgecenter/SSLTBW_2.3.0/com.ibm.zos.v2r3.gxla100/xmlssfunctions.htm)
* [Unix System Services (!= Linux On z/OS)](https://www.ibm.com/support/knowledgecenter/SSLTBW_2.3.0/com.ibm.zos.v2r3.bpx/bpx.htm)
* [XL C/C++ Compilers Documentation](https://www-01.ibm.com/support/docview.wss?uid=swg27036892)
* [XL C/C++ Compilers On IBM Marketplace](https://www.ibm.com/us-en/marketplace/xl-cpp-compiler-zos)

### ASCII<->EBCDIC Conversion

* [ASCII<->EBCDIC Conversion in Java](https://stackoverflow.com/questions/368603/convert-string-from-ascii-to-ebcdic-in-java)
* [ASCII<->EBCDIC Conv No. 2](https://edwin.baculsoft.com/2013/04/how-to-convert-from-ascii-to-ebcdic-using-java/)
* [JTOpen Toolbox](http://jt400.sourceforge.net/)
* [Software Mining Conversion Tools](https://www.softwaremining.com/)

## Misc

* [Let's Build A Compiler](https://compilers.iecc.com/crenshaw/)
* [PowerPC Notebook](https://www.powerpc-notebook.org/en/)
* [Suckless](https://suckless.org)
* [Using binfmt_misc In Order To Exec Go Files Like Scripts](https://blog.jessfraz.com/post/nerd-sniped-by-binfmt_misc/)
* [Ace Editor for the web](https://ace.c9.io/)
* [Amazon Cloud9](https://aws.amazon.com/de/cloud9/)
* [FSF High Priority Projects](https://www.fsf.org/campaigns/priority-projects/)
* [xpra](https://xpra.org/)
* [Nomachine](https://www.nomachine.com)
* [libsodium bindings](https://download.libsodium.org/doc/bindings_for_other_languages/)
* [Semantic Versioning](https://semver.org)
* [sdf](https://sdf.org/)
* [Open Standards Collection (Ada, ...)](http://www.open-std.org/)
* [Chromium Dev How-Tos](https://dev.chromium.org/developers/how-tos)
