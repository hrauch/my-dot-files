#! /bin/bash

sudo killall samba
sudo rm -rf /etc/samba/*
sudo rm -rf /var/lib/samba/*
sudo rm -rf /var/lib/krb5dc/*
sudo samba-tool domain provision --server-role=dc --use-rfc2307 --dns-backend=SAMBA_INTERNAL --realm=WSO2TEST.MYHOME --domain=WSO2TEST --adminpass=Admin123.
sudo cp /var/lib/samba/private/krb5.conf /etc
sudo cp smb.conf /etc/samba
sudo cp /etc/resolv.conf /etc/resolv.conf.orig
sudo cp smb_resolv.conf /etc/resolv.conf
sudo samba
host -t A geektop.wso2test.myhome || exit 1
smbclient -L geektop.wso2test.myhome -U% || exit 1
smbclient //geektop.wso2test.myhome/netlogon -UAdministrator%Admin123. -c 'ls' || exit 1
host -t SRV _ldap._tcp.wso2test.myhome. || exit 1
host -t SRV _kerberos._udp.wso2test.myhome. || exit 1
host -t SRV _kerberos._tcp.wso2test.myhome. || exit 1
#sudo samba-tool group create wso2test
sudo samba-tool user create smbholgi Smb123. -H ldap://geektop.wso2test.myhome -UAdministrator%Admin123.
sudo samba-tool group addmembers Administrators smbholgi -H ldap://geektop.wso2test.myhome -UAdministrator%Admin123.
sudo samba-tool spn add HTTP/geektop smbholgi
sudo samba-tool spn add HTTP/geektop.wso2test.myhome smbholgi
sudo samba-tool spn add HTTPS/geektop smbholgi
sudo samba-tool spn add HTTPS/geektop.wso2test.myhome smbholgi
sudo samba-tool spn list smbholgi





