#! /bin/bash

# stop and disable dnsmasq and avahi
# Avahi: Zeroconf/Bonjour daemon

systemctl stop docker
systemctl stop dnsmasq
#systemctl stop avahi-daemon

ifconfig virbr1 down
ifconfig docker0 down
ifconfig br-9f19f76d0e30 down
ifconfig br-99a762823e31 down
ifconfig br-3f99d75beee4 down

