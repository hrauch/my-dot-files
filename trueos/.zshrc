# vim:nospell:foldmethod=marker:foldmarker={{,}}:

export TERM=xterm-256color
export MANCOLOR=true
export EDITOR=vim
export PAGER="/usr/local/bin/most -s"
export LESS="$LESS -J"
export GREP_COLOR='01;32'
#export MANPATH=/cygdrive/c/cygwin-pkgsrc/man:~/local/share/man:$MANPATH
#export PATH=/cygdrive/c/cygwin-pkgsrc/bin:/cygdrive/c/cygwin-pkgsrc/sbin:/usr/local/sbin:/usr/local/bin:/sbin:/usr/sbin:/bin:/usr/bin:~/local/bin:$PATH:~/bin
#export MAILDIR=~/.Mail
#export TEXMFHOME=~/.texmf
#export BROWSER=google-chrome
#export http_proxy=proxy-mu.glb.intel.com:911/
#export https_proxy=proxy-mu.glb.intel.com:911/
#export ftp_proxy=proxy-mu.glb.intel.com:911/

export LANG=de_DE.UTF-8
unset LC_ALL
export LC_MESSAGES=C

export MANPATH=/usr/share/man:/usr/local/man:/root/man:/usr/share/openssl/man:/usr/local/lib/perl5/site_perl/man:/usr/local/lib/perl5/5.20/perl/man
export JAVA_HOME=/usr/local/openjdk8
export PATH=$JAVA_HOME/bin:$PATH
export MANPATH=$JAVA_HOME/man:$MANPATH
export PATH=$ANT_HOME/bin:$PATH
export PATH=~/bin:$PATH

autoload -U colors
autoload -U compinit
#autoload bashcompinit
#bashcompinit
colors
compinit
setopt NULL_GLOB
setopt auto_pushd
setopt extendedglob
setopt kshglob
setopt prompt_subst
setopt share_history

# Set a nice git-prompt (calls git_status()) {{
git_prompt_info() {
  local git_dir ref br;
  git_dir=$(git rev-parse --git-dir 2> /dev/null) || return
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  branch_prompt=${ref#refs/heads/}
  if [ -n "$branch_prompt" ]; then
    if [[ $PWD != *".git"* ]]; then
    	status_icon=$(git_status)
    fi
    echo "$status_icon%{$fg[yellow]%} [$branch_prompt]"
  fi
}

# Show character if index and/or work-tree are dirty
git_status() {
  git diff --cached --quiet
  if [[ $? == 1 ]]; then
    output="%{$fg[green]%}+"
  fi
  
  git diff --quiet
  if [[ $? == 1 ]]; then
    output="$output%{$fg[red]%}*"
  fi
  echo $output
} # }}

# {{
typeset -A HOST_COLORS
HOST_COLORS=(www cyan datenbank magenta fileserver blue guardian green)

export RPS1='$(git_prompt_info) %{$reset_color%}%*'
if [[ "$USER" == "root" ]] ; then
	export PS1='[%?]%{$fg[red]%}%n%{$reset_color%}@%m:%~# '
else
	if [[ -n "$SSH_CLIENT" ]]; then
		export PS1='[%?]%{$fg[green]%}%n%{$reset_color%}@%{$fg[yellow]%}%m%{$reset_color%}:%~\$ '
	else
		export PS1='[%?]%{$fg[green]%}%n%{$reset_color%}@%m:%~\$ '
	fi

	if [[ -n "$HOST_COLORS[$(hostname)]" ]]; then
		export PS1='[%?]%{$fg[green]%}%n%{$reset_color%}@%{$fg[$HOST_COLORS[$hostname)]]%}%m%{$reset_color%}:%~\$ '
	fi
fi

local _myhosts
_myhosts=( ${${${${(f)"$(<$HOME/.ssh/known_hosts)"}:#.*}%%\ *}%%,*} )
zstyle ':completion:*:(ssh|scp)' hosts $_myhosts
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' menu select=3

# source the bash-completion for the newest git
#if [ -f ~/.git-completion.bash ] ; then
#	source ~/.git-completion.bash
#fi #}}

# mapping of functions keys# {{
bindkey '^A'    beginning-of-line       # Home
bindkey '^E'    end-of-line             # End
bindkey '^D'    delete-char             # Del
bindkey '^R'	history-incremental-search-backward 	# reverse history search
bindkey '^[[3~' delete-char             # Del
bindkey '^[[2~' overwrite-mode          # Insert
bindkey '^[[5~' history-search-backward # PgUp
bindkey '^[[6~' history-search-forward  # PgDn
bindkey "\e\e[D" backward-word
bindkey "\e\e[C" forward-word # }}

HISTFILE="$HOME/.zsh_history"
HISTSIZE=5000
SAVEHIST=5000

#SEVEN_ZIP_BIN_PATH="/cygdrive/c/Program\ Files/7-Zip"
#VBOX_BIN_PATH="/cygdrive/c/Program\ Files/Oracle/VirtualBox"
#VBOX_VM_PATH="/cygdrive/c/Users/$LOGNAME/VirtualBox\ VMs"
alias reinstallcentos57="cd $VBOX_VM_PATH && rm -rf CentOS-5.7 && $SEVEN_ZIP_BIN_PATH/7z.exe x CentOS-5.7-i386.7z"
#alias startcentos57="$VBOX_BIN_PATH/VBoxHeadless --startvm CentOS-5.7 &"
alias startcentos57="$VBOX_BIN_PATH/VBoxManage startvm CentOS-5.7 --type headless"
alias stopcentos57="$VBOX_BIN_PATH/VBoxManage controlvm CentOS-5.7 poweroff"
alias snapcentos57="$VBOX_BIN_PATH/VBoxManage snapshot CentOS-5.7 take mysnap"
alias restsnapcentos57="$VBOX_BIN_PATH/VBoxManage snapshot CentOS-5.7 restore mysnap"
alias restsnapinstpkgscentos57="$VBOX_BIN_PATH/VBoxManage snapshot CentOS-5.7 restore mysnap_pkgs_installed"
alias initcentos57="$VBOX_BIN_PATH/VBoxManage modifyvm CentOS-5.7 --memory 1536 --nictype1 82545EM --natpf1 sshrule,tcp,,2222,,22 --natpf1 plainhttprule,tcp,,8080,,80 --natpf1 httpsrule,tcp,,8443,,443"

alias vi=vim
alias ls='ls -FG'
alias ll='ls -FGl'
alias la='ls -FGA'
alias l='ls -CFG'
alias rm="rm -i"
alias mv="mv -i"
alias cp_p='rsync -WavP'

alias lte="sudo /usr/local/sbin/lte.sh"
alias hidrsftp="sftp hiholgi@sftp.hidrive.strato.com:/users/hiholgi"
alias lp="/usr/local/bin/lp -d hpclj-mfp-m476dw "
alias tmux="tmux -2"
alias nvmu="nvm use --delete-prefix "


alias gdb='gdb -q'
alias dirsize='tee /tmp/du.txt|grep -E "[0123456789]M"|sort -n ; grep -E "[0123456789]G" /tmp/du.txt| sort -n; rm -rf /tmp/du.txt'
alias myip='lynx -dump http://www.whatismyip.org/'
alias config='git --git-dir=$HOME/.config.git/ --work-tree=$HOME'
#alias llgal_prepare='mkdir -p .llgal; for file in *.{jpg,JPG,png};do convert -resize 800x600 $file .llgal/scaled_$file; convert -resize 160x85 $file .llgal/thumb_$file; done'

case $TERM in
	xterm|rxvt|rxvt-unicode|rxvt-256color)
        precmd () {print -Pn "\e]0;%n@%m: %~\a"}
	alias news='TERM=xterm-256color slrn -n -C -h news.cis.dfn.de'
	alias gmane='TERM=xterm-256color slrn -n -C -h news.gmane.org -f ~/.gmanerc'
	alias halifax='TERM=xterm-256color slrn -n -C -h news.halifax.rwth-aachen.de -f ~/.halifaxrc'
	alias mutt='TERM=xterm-256color mutt'
	;;
esac
mutti () { mutt -F ~/.mutt/profile.$* }

pdfgrep () {
	for file in $*; do
		echo "=== $file ==="
		pdftotext $file - | grep --color -i $1
	done
}

git_sync () {
	CURDIR=$PWD
	for dir in $(find $* -name "*.git" -type d)
	do
		cd $CURDIR/$dir/../ && if [[ -n $(git remote) ]]
		then
			echo "*** Pulling in dir $PWD" && git pull -q && git gc --quiet; 
		fi
	done
	cd $CURDIR
	echo "*** All done!"
}

git_gc () {
	CURDIR=$PWD
	for dir in $(find $* -name "*.git" -type d)
	do
		cd $CURDIR/$dir/../
		echo "*** GC in dir $PWD" && git gc --quiet
	done
	cd $CURDIR
	echo "*** All done!"
}

man() {
	env \
		LESS_TERMCAP_mb=$(printf "\e[1;31m") \
		LESS_TERMCAP_md=$(printf "\e[1;31m") \
		LESS_TERMCAP_me=$(printf "\e[0m") \
		LESS_TERMCAP_se=$(printf "\e[0m") \
		LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
		LESS_TERMCAP_ue=$(printf "\e[0m") \
		LESS_TERMCAP_us=$(printf "\e[1;32m") \
			man "$@"
}



#	alias screenstandby='sleep 1; xset dpms force standby'
#	alias hibernate_sync='su jojo -c "purple-remote setstatus\?status\=offline"; sync; echo 3 > /proc/sys/vm/drop_caches; su jojo -c "gnome-screensaver-command -l"; hibernate; su jojo -c "purple-remote setstatus\?status\=available"; su jojo -c "killall -USR1 offlineimap"; ntpdate timeserver.rwth-aachen.de'
#	alias suspend_sync='su jojo -c "xscreensaver-command -lock"; pm-suspend'

#alias rawconv='ufraw-batch --wb=camera --size=2816 --out-type=jpeg --compression=100'

	#alias keyboard='xmodmap ~/.Xmodmap; xset r rate 500 50; xset dpms 1800 3600; xset m 0.9/1; xset r 113; xset r 116; xset +dpms'
	#alias single_monitor='xrandr --output HDMI2 --auto --output HDMI2 --primary --output HDMI1 --off --output LVDS1 --off; keyboard'
	#alias monitor_dock='xrandr --output HDMI2 --auto --output HDMI2 --primary --output HDMI1 --off --output LVDS1 --auto --output LVDS1 --right-of HDMI2; keyboard'

	#alias monitors='xrandr --output HDMI2 --auto; xrandr --output LVDS1 --off; xrandr --output HDMI1 --auto --output HDMI2 --auto --output HDMI2 --primary --output HDMI1 --right-of HDMI2; xset dpms force off; xset dpms force on; xsetroot -solid grey6; keyboard'
	#alias undock='DISPLAY=:0.0 xrandr --output HDMI2 --off --output HDMI1 --off; DISPLAY=:0.0 xrandr --output LVDS1 --auto; DISPLAY=:0.0 xset dpms force off; DISPLAY=:0.0 xset dpms force on; xsetroot -solid grey6'
	#alias cpu_save='echo powersave > /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor'
	#alias cpu_demand='echo "ondemand" > /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor'

#	export LSCOLORS="cxfxcxdxbxegedabagacad"
#	alias ls='ls -G -F'
#	alias exiftool='PERL5LIB=/sw/lib/perl5 exiftool'
#	alias temp='/Applications/TemperatureMonitor.app/Contents/MacOS/tempmonitor -a -c -l'
#
#	alias ntp-update="sudo /System/Library/StartupItems/NetworkTime/NetworkTime restart"
#	alias hibernate="osascript -e 'tell application \"Finder\" to sleep' && sleep 5 && sudo /System/Library/StartupItems/NetworkTime/NetworkTime restart"
#	alias xlock='open /System/Library/Frameworks/ScreenSaver.framework/Resources/ScreenSaverEngine.app'
#
#else
#	eval "`/bin/dircolors -b ~/.dircolors`"
#	alias ls='ls --color=auto -F'
#fi

#export PATH=~/bin:~/.yarn/bin:/usr/local/gcc6-aux/bin:$PATH
export PATH=~/bin:~/.yarn/bin:$PATH:/usr/local/gcc6-aux/bin:/usr/local/dmd2/freebsd/bin64

alias lsxkbmodels="sed '/^! model$/,/^ *$/!d;//d' /usr/local/share/X11/xkb/rules/base.lst"
alias lsxkblayouts="sed '/^! layout$/,/^ *$/!d;//d' /usr/local/share/X11/xkb/rules/base.lst"
alias lsxkbvariants="sed '/^! variant$/,/^ *$/!d;//d' /usr/local/share/X11/xkb/rules/base.lst"
alias lsxkboptions="sed '/^! option$/,/^ *$/!d;//d' /usr/local/share/X11/xkb/rules/base.lst"

alias viewxkb="less -M '+/^\s*\!\s\w+$' /usr/local/share/X11/xkb/rules/base.lst"
alias viewxkbmodels="lsxkbmodels | less -M"
alias viewxkblayouts="lsxkblayouts | less -M"
alias viewxkbvariants="lsxkbvariants | less -M"
alias viewxkboptions="lsxkboptions | less -M"

alias gpg=/usr/local/bin/gpg2

# install-tl
export TEXARCH="amd64"
export TEXOS="freebsd"
#export TEXARCH="x86_64"
#export TEXOS="linux"
export TEXOSFULL="${TEXARCH}-$TEXOS"
export TEXREL="2017"
export TEXDIR="/usr/local/texlive/$TEXREL"
export TEXMFLOCAL="~/.texlive/$TEXREL/texmf-local"
export TEXMFSYSCONFIG="$TEXDIR/texmf-config"
export TEXMFSYSVAR="$TEXDIR/texmf-var"
export TEXMFCONFIG="$TEXMFSYSCONFIG"
export TEXMFHOME="$TEXMFLOCAL"
export TEXMFVAR="~/.texlive/$TEXREL/texmf-var"
export PATH="$TEXDIR/bin/$TEXOSFULL:$PATH"
export MANPATH="$TEXDIR/texmf-dist/doc/man:$MANPATH"
export INFOPATH="$TEXDIR/texmf-dist/doc/info:$INFOPATH"
export PATH=/usr/local/texlive/2017/bin/x86_64-linux:$PATH
export MANPATH=/usr/local/texlive/2017/texmf-dist/doc/man:$MANPATH
export INFOPATH=/usr/local/texlive/2017/texmf-dist/doc/info:$INFOPATH

# Heirloom Doctools
export PATH=/usr/local/heirloom-doctools/bin:$PATH
export MANPATH=/usr/local/heirloom-doctools/man:$MANPATH

# Utroff (addon stuff for Heirloom Doctools)
export PATH=/usr/local/utroff/bin:$PATH
export MANPATH=/usr/local/utroff/man:$MANPATH

export NVM_DIR="/usr/home/holgi/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/usr/home/holgi/.sdkman"
[[ -s "/usr/home/holgi/.sdkman/bin/sdkman-init.sh" ]] && source "/usr/home/holgi/.sdkman/bin/sdkman-init.sh"
