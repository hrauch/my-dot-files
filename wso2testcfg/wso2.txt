WSO2
====

* mehr als 500 Mitarbeiter
* Hauptsitz in USA (Mountain View, CA bzw. NY)
* Niederlassungen in Sao Paulo, London, Syndney, Colombo, Jaffna
* Umfangreiches Produkt-Portfolio rund um Web Services auf Basis von
  Carbon als gemeinsames framework (siehe:
  https://wso2.com/wso2-documentation)
* Alle Produkte sind unter der Apache-Lizenz freigegeben und basieren auf
  in der Praxis bewährten Open-Source-Projekten (Tomcat, OpenJPA, ...)
* Alle Produkte bei github open sourced
* Es gibt div. Fallstudien zum Einsatz in Behörden/größeren Organisationen
  mit mehreren Mio. Nutzern
* Sowohl kommerzieller (WSO2, Yenlo) als auch nicht-kommerzieller
  (StackOverflow, Blog-Artikel, ...) verfügbar
